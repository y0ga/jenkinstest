package com.herokuapp.todomvc4tasj.features;

import com.codeborne.selenide.CollectionCondition;
import com.herokuapp.todomvc4tasj.categories.Acceptance;
import com.herokuapp.todomvc4tasj.categories.Smoke;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.herokuapp.todomvc4tasj.features.ToDoMVC.*;

/**
 * Created by viktor on 2/29/2016.
 */
public class ToDoMVCLifecycleTest extends BaseTest {

    @Test
    @Category({Smoke.class, Acceptance.class})
    public void testTasksLifeCycle() {
        givenAtAll();

        add("a");
        startEdit("a", "a edited").pressEnter();

        // complete
        toggle("a edited");
        assertTasks("a edited");

        filterActive();
        assertNoTasks();

        add("b");
        assertTasks("b");
        assertItemsLeft(1);

        // complete
        toggleAll();
        assertNoTasks();

        filterCompleted();
        assertTasks("a edited", "b");

        // reopen
        toggle("b");
        assertTasks("a edited");

        clearCompleted();
        assertNoTasks();

        filterAll();
        delete("b");
        tasks.shouldBe(CollectionCondition.empty);

    }

}
