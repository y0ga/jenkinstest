package com.herokuapp.todomvc4tasj.features;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

/**
 * Created and edited by viktor on 3/31/2016.
 */
public class ToDoMVC {

    public static ElementsCollection tasks = $$("#todo-list>li");
    public static SelenideElement newTask = $("#new-todo");
    public static String baseUrl = "https://todomvc4tasj.herokuapp.com";

    @Step
    public static void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public static void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public static void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    public static void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    public static void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    public static void add(String... taskTexts) {
        for (String text : taskTexts) {
            newTask.shouldBe(enabled).setValue(text).pressEnter();
        }
    }

    @Step
    public static void assertTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public static void assertNoTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    public static void toggle(String taskText) {
        tasks.find(exactText(taskText)).$(".toggle").click();
    }

    @Step
    public static void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public static void assertItemsLeft(int activeTasksCount) {
        $("#todo-count>strong").shouldHave(exactText(Integer.toString(activeTasksCount)));
    }

    @Step
    public static SelenideElement startEdit(String oldTaskText, String NewTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.findBy(cssClass("editing")).$(".edit").setValue(NewTaskText);
    }


    public enum TaskType {
        ACTIVE("false"), COMPLETED("true");

        public String flag;

        TaskType(String flag) {
            this.flag = flag;
        }

        public String getFlag() {
            return flag;
        }
    }


    public static class Task {

        TaskType taskType;
        String taskText;

        public Task(String taskText, TaskType taskType) {
            this.taskText = taskText;
            this.taskType = taskType;
        }
    }

    public static Task aTask(TaskType taskType, String taskText) {
        return new Task(taskText, taskType);
    }

    @Step
    public static void givenAtAll(Task... tasks) {
        if (!(url().equals(baseUrl)))
            open(baseUrl);

        String script = "localStorage.setItem('todos-troopjs', '[";

        for (Task task : tasks) {
            script += "{\"completed\":" + task.taskType.getFlag() + ", \"title\":\"" + task.taskText + "\"},";
        }

        if (tasks.length != 0) {
            script = script.substring(0, (script.length() - 1));
        }
        script = script + "]');";

        executeJavaScript(script);
        refresh();
    }

    @Step
    public static void givenAtActive(Task... tasks) {
        givenAtAll(tasks);
        filterActive();
    }

    @Step
    public static void givenAtCompleted(Task... tasks) {
        givenAtAll(tasks);
        filterCompleted();
    }

    @Step
    public static void givenAtAll(TaskType taskType, String... taskTexts) {
        Task[] tasks = new Task[taskTexts.length];
        for (int i = 0; i < taskTexts.length; i++)
            tasks[i] = aTask(taskType, taskTexts[i]);
        givenAtAll(tasks);

    }

    @Step
    public static void givenAtActive(TaskType taskType, String... taskTexts) {
        givenAtAll(taskType, taskTexts);
        filterActive();
    }

    @Step
    public static void givenAtCompleted(TaskType taskType, String... taskTexts) {
        givenAtAll(taskType, taskTexts);
        filterCompleted();
    }
}
