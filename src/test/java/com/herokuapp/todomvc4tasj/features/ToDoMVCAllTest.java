package com.herokuapp.todomvc4tasj.features;

import com.herokuapp.todomvc4tasj.categories.Acceptance;
import com.herokuapp.todomvc4tasj.categories.Buggy;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.herokuapp.todomvc4tasj.features.ToDoMVC.TaskType.ACTIVE;
import static com.herokuapp.todomvc4tasj.features.ToDoMVC.TaskType.COMPLETED;
import static com.herokuapp.todomvc4tasj.features.ToDoMVC.*;

/**
 * Created by viktor on 3/23/2016.
 */
public class ToDoMVCAllTest extends BaseTest {

    @Test
    @Category(Acceptance.class)
    public void testCompleteAll() {
        givenAtAll(ACTIVE, "a", "b", "c");

        toggleAll();
        assertTasks("a", "b", "c");
        assertItemsLeft(0);
    }

    @Test
    @Category(Acceptance.class)
    public void testReopen() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"), aTask(ACTIVE, "c"));

        toggle("b");
        assertTasks("a", "b", "c");
        assertItemsLeft(3);
    }

    @Test
    @Category(Acceptance.class)
    public void testClearCompleted() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"), aTask(COMPLETED, "c"));

        clearCompleted();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    @Category(Acceptance.class)
    public void testTransitionToCompleted() {
        givenAtAll(aTask(COMPLETED, "a"), aTask(ACTIVE, "b"));

        filterCompleted();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    @Category(Acceptance.class)
    public void testConfirmEditByTab() {
        givenAtAll(ACTIVE, "a");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    @Category(Buggy.class)
    public void testCancelEditByEsc() {
        givenAtAll(aTask(ACTIVE, "b"), aTask(COMPLETED, "c"), aTask(ACTIVE, "d"));

        startEdit("b", "b editing cancelled").pressEscape();

        //buggy step
        assertNoTasks();

        assertItemsLeft(2);
    }
}
